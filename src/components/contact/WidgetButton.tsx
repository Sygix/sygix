import { motion } from 'framer-motion';
import React, { useEffect, useRef, useState } from 'react';
import { FiMessageSquare, FiX } from 'react-icons/fi';

import Form from '@/components/contact/Form';

const variants = {
  open: { opacity: 1, x: 0 },
  closed: { opacity: 0, x: '100%' },
};

function WidgetButton() {
  const openWidget = () => {
    setInAnimation(true);
    setOpen(!isOpen);
  };
  const form = useRef(null);
  const [isOpen, setOpen] = useState(false);
  const [inAnimation, setInAnimation] = useState(false);

  useEffect(() => {
    if (!isOpen) return;
    function handleClick(event: MouseEvent) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (form.current && !form.current.contains(event.target)) {
        setInAnimation(true);
        setOpen(false);
      }
    }
    window.addEventListener('click', handleClick);
    return () => window.removeEventListener('click', handleClick);
  }, [isOpen]);

  return (
    <div className='fixed right-5 bottom-5 z-30 flex flex-col items-end sm:max-h-full'>
      <motion.div
        ref={form}
        className={`${
          !inAnimation && !isOpen ? 'hidden' : ''
        } fixed top-0 left-0 z-50 h-full w-screen overflow-auto bg-neutral-700/90 p-5 backdrop-blur-lg sm:static sm:my-2 sm:h-auto sm:w-auto sm:rounded-3xl sm:shadow-lg`}
        animate={isOpen ? 'open' : 'closed'}
        initial={variants.closed}
        variants={variants}
        onAnimationStart={() => setInAnimation(true)}
        onAnimationComplete={() => setInAnimation(false)}
      >
        <Form openWidget={openWidget} />
      </motion.div>
      {/* {openContactForm && <Form openWidget={openWidget} />} */}
      <button
        onClick={openWidget}
        className={`${
          isOpen ? '' : 'lg:animate-bounce'
        } flex h-14 w-14 items-center justify-center rounded-full bg-neutral-700/30 shadow-lg backdrop-blur-lg transition duration-300 hover:bg-neutral-700/90 focus:bg-neutral-700/90 focus:outline-none`}
      >
        {isOpen ? (
          <FiX className='h-6 w-6 text-white' />
        ) : (
          <FiMessageSquare className='h-6 w-6 text-white' />
        )}
      </button>
    </div>
  );
}

export default WidgetButton;
