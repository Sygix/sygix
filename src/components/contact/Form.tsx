import { yupResolver } from '@hookform/resolvers/yup';
import { useState } from 'react';
import { FieldValues, useForm } from 'react-hook-form';
import * as Yup from 'yup';

import styles from './Form.module.css';

export default function Form(props: { openWidget: () => void }) {
  // form validation rules
  const validationSchema = Yup.object().shape({
    fullName: Yup.string()
      .required('Full name is required')
      .min(2, 'Your name should be longer than 2 letters')
      .max(30, 'Your name is too long')
      .matches(/^[a-z\s]+$/i, 'Only alphabets are allowed'),
    email: Yup.string().required('Email is required').email('Email is invalid'),
    message: Yup.string()
      .required('Your message is required')
      .min(30, 'Your message is too short')
      .max(10000, 'Your message is too long')
      .matches(
        /^[a-zÀ-ÖØ-öø-ÿ0-9,.\s]+$/i,
        'Special characters are not allowed'
      ),
  });

  // get functions to build form with useForm() hook
  const { register, handleSubmit, formState, setError, trigger } = useForm({
    mode: 'onChange',
    resolver: yupResolver(validationSchema),
  });
  const { errors } = formState;
  const [fetchStatus, setFetchStatus] = useState(0);

  async function onSubmit(data: FieldValues) {
    setFetchStatus(100);
    await fetch('/api/contact', {
      method: 'post',
      body: JSON.stringify(data),
    })
      .then((r) => {
        if (r.status != 200) {
          setError('serverError', {
            type: 'manual',
            message: r.statusText,
          });
          trigger();
        }
        setFetchStatus(r.status);
      })
      .catch(() => {
        setError('serverError', {
          type: 'manual',
          message: 'An error happened, please try again later...',
        });
        trigger();
        setFetchStatus(500);
      });
  }

  return (
    <>
      <div className='text-center'>
        <h4 className='my-3 text-3xl font-semibold text-gray-200'>
          Contact Me
        </h4>
        <p className='text-gray-200'>
          Fill up the form below to send me a message.
        </p>
      </div>
      <div className='m-7'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='mb-6'>
            <label htmlFor='fullName' className={styles.label}>
              Full Name
            </label>
            <input
              type='text'
              placeholder='John Doe'
              required
              {...register('fullName')}
              className={styles.input}
            />
            <p className={styles.errorMessage}>{errors.fullName?.message}</p>
          </div>
          <div className='mb-6'>
            <label htmlFor='email' className={styles.label}>
              Email
            </label>
            <input
              type='text'
              placeholder='you@gmail.com'
              required
              {...register('email')}
              className={styles.input}
            />
            <p className={styles.errorMessage}>{errors.email?.message}</p>
          </div>
          <div className='mb-6'>
            <label htmlFor='message' className={styles.label}>
              Your Message
            </label>
            <textarea
              rows={5}
              placeholder='Your Message'
              required
              {...register('message')}
              className={styles.input}
            />
            <p className={styles.errorMessage}>{errors.message?.message}</p>
          </div>
          <div className='mb-6'>
            <button
              disabled={
                !formState.isDirty ||
                !formState.isValid ||
                formState.isSubmitSuccessful
              }
              type='submit'
              className='ease w-full rounded-md bg-neutral-800 px-3 py-4 text-white transition duration-300 hover:bg-neutral-900 focus:bg-neutral-900 focus:outline-none disabled:bg-neutral-500'
            >
              Send Message
            </button>
          </div>
          <div className='mb-6'>
            {fetchStatus === 100 ? (
              <p className='text-center text-orange-300'>
                Your message is being sent... Please wait a few seconds.
              </p>
            ) : fetchStatus === 200 ? (
              <p className='text-center text-green-300'>
                Your message has been sent successfully.
              </p>
            ) : fetchStatus === 400 ? (
              <p className='text-center text-red-300'>
                Please check the fields before sending your message.
              </p>
            ) : fetchStatus === 0 ? (
              ''
            ) : (
              <p className='text-center text-red-300'>
                An error happened, please try again later...
              </p>
            )}
          </div>
          <div className='mb-6 sm:hidden'>
            <button
              onClick={(e) => {
                e.preventDefault();
                props.openWidget();
              }}
              className='ease w-full rounded-md bg-neutral-800 px-3 py-4 text-white transition duration-300 hover:bg-neutral-900 focus:bg-neutral-900 focus:outline-none'
            >
              Close
            </button>
          </div>
        </form>
      </div>
    </>
  );
}
