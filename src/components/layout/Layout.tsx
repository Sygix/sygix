import { motion, useInView } from 'framer-motion';
import Image from 'next/image';
import * as React from 'react';
import { useEffect, useRef, useState } from 'react';

import style from './Layout.module.css';

import clsxm from '@/lib/clsxm';

import WidgetButton from '@/components/contact/WidgetButton';
import Menu from '@/components/layout/Menu';

const logoVariants = {
  onView: {
    rotate: [0, 360],
    transition: {
      repeat: Infinity,
      repeatDelay: 1,
      duration: 2,
      ease: 'easeInOut',
    },
  },
};

export default function Layout({ children }: { children: React.ReactNode }) {
  const logo = useRef(null);
  const logoIsInView = useInView(logo);
  const [animateBg, setAnimateBg] = useState(false);

  useEffect(() => {
    const animateBgData = localStorage.getItem('animateBg');
    if (animateBgData !== null) setAnimateBg(JSON.parse(animateBgData));
  }, []);

  useEffect(() => {
    localStorage.setItem('animateBg', JSON.stringify(animateBg));
  }, [animateBg]);

  return (
    <>
      <div
        className={clsxm(
          'flex flex-col overflow-hidden antialiased',
          style.bgGradient,
          animateBg && style.animatedBgGradient
        )}
      >
        <Menu
          animateBg={animateBg}
          setAnimateBg={() => setAnimateBg(!animateBg)}
        />
        <motion.div
          ref={logo}
          className='relative z-10 flex h-28 w-full items-center justify-center'
          animate={logoIsInView ? logoVariants.onView : ''}
          variants={logoVariants}
        >
          <Image
            className={clsxm(style.logo, 'shadow-black drop-shadow-lg')}
            layout='fill'
            src='/images/SygixLogoBW.svg'
            alt='logo Sygix'
          />
        </motion.div>
        <main className='flex w-full'>{children}</main>
        <WidgetButton />
      </div>
    </>
  );
}
