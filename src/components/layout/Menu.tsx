import { motion } from 'framer-motion';
import Link from 'next/link';
import React, { useEffect, useRef, useState } from 'react';
import {
  FaBars,
  FaGithub,
  FaGitlab,
  FaHome,
  FaTelegramPlane,
} from 'react-icons/fa';
import { SiIpfs } from 'react-icons/si';

import clsxm from '@/lib/clsxm';

import Switch from '../buttons/Switch';

const variants = {
  open: { opacity: 1, x: 0 },
  closed: { opacity: 0, x: '-100%' },
};

function Menu(props: { animateBg: boolean; setAnimateBg: () => void }) {
  const menu = useRef(null);
  const [isOpen, setOpen] = useState(false);
  const [inAnimation, setInAnimation] = useState(false);

  useEffect(() => {
    if (!isOpen) return;
    function handleClick(event: MouseEvent) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (menu.current && !menu.current.contains(event.target)) {
        setInAnimation(true);
        setOpen(false);
      }
    }
    window.addEventListener('click', handleClick);
    return () => window.removeEventListener('click', handleClick);
  }, [isOpen]);

  return (
    <>
      <div className='fixed left-5 top-5 z-40'>
        <button
          className='flex items-center rounded-full bg-neutral-700/30 p-4 text-white shadow-lg backdrop-blur-lg transition duration-300 hover:bg-neutral-700/90 focus:bg-neutral-700/90 focus:outline-none'
          onClick={() => setOpen(!isOpen)}
        >
          <FaBars className='h-6 w-6' />
          <p className='max-w-0 overflow-x-hidden sm:ml-3 sm:max-w-full'>
            Menu
          </p>
        </button>

        {/*Menu content*/}
        <motion.div
          ref={menu}
          className={`${
            !inAnimation && !isOpen ? 'hidden' : ''
          } my-2 rounded-3xl bg-neutral-700/90 p-5 text-white shadow-lg backdrop-blur-lg`}
          animate={isOpen ? 'open' : 'closed'}
          initial={variants.closed}
          variants={variants}
          onAnimationStart={() => setInAnimation(true)}
          onAnimationComplete={() => setInAnimation(false)}
        >
          <nav>
            <p className='p-2 text-center'>Sygix - FullStack Developer</p>
            <hr className='my-2' />
            <ul className='space-y-2'>
              <li>
                <Link href='/'>
                  <a className='flex items-center rounded-lg p-2 transition duration-300 hover:bg-white/10'>
                    <FaHome className='h-6 w-6' />
                    <span className='ml-3'>Home</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href='/ipfs'>
                  <a className='flex items-center rounded-lg p-2 transition duration-300 hover:bg-white/10'>
                    <SiIpfs className='h-6 w-6' />
                    <span className='ml-3'>IPFS Gateway</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href='https://gitlab.com/Sygix'>
                  <a className='flex items-center rounded-lg p-2 transition duration-300 hover:bg-white/10'>
                    <FaGitlab className='h-6 w-6' />
                    <span className='ml-3'>GitLab</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href='https://github.com/Sygix'>
                  <a className='flex items-center rounded-lg p-2 transition duration-300 hover:bg-white/10'>
                    <FaGithub className='h-6 w-6' />
                    <span className='ml-3'>GitHub</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href='https://telegram.me/Sygix'>
                  <a className='flex items-center rounded-lg p-2 transition duration-300 hover:bg-white/10'>
                    <FaTelegramPlane className='h-6 w-6' />
                    <span className='ml-3'>Telegram</span>
                  </a>
                </Link>
              </li>
              <li className='flex items-center p-2'>
                <Switch
                  className={clsxm(
                    'text-3xl',
                    props.animateBg && '!bg-green-500/60'
                  )}
                  isDark={true}
                  toggleIsOn={props.animateBg}
                  toggle={props.setAnimateBg}
                />
                <span className='ml-3'>Background</span>
              </li>
            </ul>
          </nav>
          <div>
            <hr className='my-2' />
            <p className='p-2 text-center text-xs'>
              Made with ❤ by Sygix © {new Date().getFullYear()}
            </p>
          </div>
        </motion.div>
      </div>
    </>
  );
}

export default Menu;
