import { motion } from 'framer-motion';
import * as React from 'react';
import { useState } from 'react';
import { RiAlarmWarningFill } from 'react-icons/ri';

import ArrowLink from '@/components/links/ArrowLink';
import Seo from '@/components/Seo';

export default function NotFoundPage() {
  const [notFoundText, setNotFoundText] = useState('Page Not Found');

  return (
    <>
      <Seo templateTitle='Page Not Found' />
      <div
        className='flex w-full items-center justify-center'
        style={{
          height: 'calc(100vh - 7rem)',
        }}
      >
        <motion.div
          className='layout flex h-full cursor-grab flex-col items-center justify-center text-center text-white'
          drag
          onDragStart={() => setNotFoundText('YAAAAAAAY !')}
          onDragEnd={() => setNotFoundText('Page Not Found')}
          dragSnapToOrigin={true}
          whileDrag={{ scale: 1.2 }}
          dragConstraints={{
            top: -200,
            left: -200,
            right: 200,
            bottom: 200,
          }}
        >
          <RiAlarmWarningFill
            size={60}
            className='drop-shadow-glow animate-flicker text-red-500'
          />
          <h1 className='mt-8 text-4xl md:text-6xl'>{notFoundText}</h1>
          <ArrowLink className='mt-4 border-white md:text-lg' href='/'>
            Back to Home
          </ArrowLink>
        </motion.div>
      </div>
    </>
  );
}
