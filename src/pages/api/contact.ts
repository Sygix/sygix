import mail from '@sendgrid/mail';
import { NextApiRequest, NextApiResponse } from 'next';
import { object, string } from 'yup';

const SENDGRID_API_KEY: string | undefined = process.env.SENDGRID_API_KEY;
if (SENDGRID_API_KEY != null) {
  mail.setApiKey(SENDGRID_API_KEY);
}

export default function contact(req: NextApiRequest, res: NextApiResponse) {
  return new Promise<void>((resolve) => {
    try {
      const bodySchema = object({
        fullName: string()
          .required()
          .min(2)
          .matches(/^[aA-zZ\s]+$/),
        email: string().email(),
        message: string()
          .required()
          .min(30)
          .matches(/^[a-zÀ-ÖØ-öø-ÿ0-9,.\s]+$/i),
      });

      bodySchema
        .validate(JSON.parse(req.body))
        .then((parsedBody) => {
          const message = `
                    Name: ${parsedBody.fullName}\r\n
                    Email: ${parsedBody.email}\r\n
                    Message: ${parsedBody.message}
                `;

          const data = {
            to: 'contact@sygix.fr',
            from: 'contact@sygix.fr',
            subject: `Nouveau message de ${parsedBody.fullName}`,
            text: message,
            html: message.replace(/\r\n/g, '<br />'),
          };
          mail
            .send(data)
            .then(() => {
              res.status(200).json({ status: 'OK' });
              resolve();
            })
            .catch(() => {
              res.status(500).json({ status: 'Server Error' });
              resolve();
            });
        })
        .catch(() => {
          res.status(400).json({ status: 'Bad Request' });
          resolve();
        });
    } catch (e) {
      res.status(500).json({ status: 'Server Error' });
      resolve();
    }
  });
}
