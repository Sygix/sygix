import { motion, useInView, Variants } from 'framer-motion';
import Link from 'next/link';
import * as React from 'react';
import { useEffect, useRef } from 'react';
import {
  FaAngular,
  FaGit,
  FaLinux,
  FaNodeJs,
  FaPhp,
  FaReact,
  FaWordpress,
} from 'react-icons/fa';
import {
  SiCss3,
  SiGraphql,
  SiHtml5,
  SiJquery,
  SiNextdotjs,
  SiStrapi,
  SiTailwindcss,
  SiTypescript,
} from 'react-icons/si';

import style from './index.module.css';

import clsxm from '@/lib/clsxm';

import NextImage from '@/components/NextImage';
import Seo from '@/components/Seo';

const mockupVariants: Variants = {
  offscreen: {
    y: 300,
  },
  onscreen: {
    y: 0,
    transition: {
      type: 'spring',
      bounce: 0.4,
      duration: 0.8,
    },
  },
};

const scrollVariants = {
  onView: {
    x: [0, -1000, -1000, -1000, 0],
    transition: {
      repeat: Infinity,
      duration: 20,
      delay: 5,
      repeatDelay: 5,
      ease: 'easeInOut',
    },
  },
};

export default function HomePage() {
  const neho = useRef(null);
  const nehoIsInView = useInView(neho);

  useEffect(() => {
    function updateSize() {
      if (neho.current) {
        scrollVariants.onView = {
          x: [
            0,
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            -(neho.current.offsetWidth + 16),
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            -(neho.current.offsetWidth + 16),
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            -(neho.current.offsetWidth + 16),
            0,
          ],
          transition: {
            repeat: Infinity,
            duration: 20,
            delay: 5,
            repeatDelay: 5,
            ease: 'easeInOut',
          },
        };
      }
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  });

  return (
    <>
      <Seo />

      <div className='flex w-full flex-col items-center'>
        <section className={style.presentation}>
          <p>
            <span>I&apos;m Sygix</span>, a full stack web Developer. I love
            building awsome websites/apps and discovering new tech (currently
            onto web3 🤙). Usually, I use tools like <span>React, NextJS</span>{' '}
            and Angular.
          </p>
          <br></br>
          <p>
            Feel free to reach out to me using the form at the bottom of this
            page or via my Telegram. I&apos;ll do my best to get back to you as
            soon as possible. 😊
          </p>
        </section>

        <section className={style.skills}>
          <h2>My skills</h2>
          <div className={style.skillsContainer}>
            <div className={style.skill}>
              <FaReact className={style.skillSVG} />
              <p>React</p>
            </div>
            <div className={style.skill}>
              <SiNextdotjs className={style.skillSVG} />
              <p>NextJS</p>
            </div>
            <div className={style.skill}>
              <FaAngular className={style.skillSVG} />
              <p>Angular</p>
            </div>
            <div className={style.skill}>
              <SiTypescript className={style.skillSVG} />
              <p>Typescript</p>
            </div>
            <div className={style.skill}>
              <FaNodeJs className={style.skillSVG} />
              <p>NodeJS</p>
            </div>
            <div className={style.skill}>
              <SiStrapi className={style.skillSVG} />
              <p>Strapi</p>
            </div>
            <div className={style.skill}>
              <SiGraphql className={style.skillSVG} />
              <p>GraphQL</p>
            </div>
            <div className={style.skill}>
              <FaPhp className={style.skillSVG} />
              <p>PHP</p>
            </div>
            <div className={style.skill}>
              <SiHtml5 className={style.skillSVG} />
              <p>HTML</p>
            </div>
            <div className={style.skill}>
              <SiCss3 className={style.skillSVG} />
              <p>CSS</p>
            </div>
            <div className={style.skill}>
              <SiTailwindcss className={style.skillSVG} />
              <p>TailwindCSS</p>
            </div>
            <div className={style.skill}>
              <FaGit className={style.skillSVG} />
              <p>Git</p>
            </div>
            <div className={style.skill}>
              <FaLinux className={style.skillSVG} />
              <p>Linux</p>
            </div>
          </div>
        </section>

        <section className={style.projects}>
          <h2>My work</h2>

          {/*AssettoHosting project*/}
          <motion.div
            className={style.projectContainer}
            initial='offscreen'
            whileInView='onscreen'
            viewport={{ once: true, amount: 0.8 }}
          >
            <motion.figure
              className={clsxm(style.projectMockupBg, 'bg-assettohosting/40')}
              variants={mockupVariants}
            >
              <Link href='https://assettohosting.com/'>
                <a>
                  <NextImage
                    className={style.projectMockup}
                    layout='responsive'
                    width={1896}
                    height={999}
                    src='/images/assettohosting-mockup.png'
                    alt='screenshot of AssettoHosting website'
                  />
                </a>
              </Link>
            </motion.figure>

            <div className={style.projectDescContainer}>
              <h3 className={style.projectTitle}>AssettoHosting</h3>
              <time className={style.projectTime} dateTime='2023'>
                2023
              </time>

              <div className={style.projectTech}>
                <SiNextdotjs className={style.techSVG} />
                <SiStrapi className={style.techSVG} />
                <SiGraphql className={style.techSVG} />
                <SiTailwindcss className={style.techSVG} />
              </div>
            </div>
          </motion.div>

          {/*Bobines et Mélusine project*/}
          <motion.div
            className={style.projectContainer}
            initial='offscreen'
            whileInView='onscreen'
            viewport={{ once: true, amount: 0.8 }}
          >
            <motion.figure
              className={clsxm(
                style.projectMockupBg,
                'bg-bobinesetmelusine/40'
              )}
              variants={mockupVariants}
            >
              <Link href='https://www.bobinesetmelusine.fr/'>
                <a>
                  <NextImage
                    className={style.projectMockup}
                    layout='responsive'
                    width={1902}
                    height={1002}
                    src='/images/bobines-et-melusine-mockup.png'
                    alt='screenshot of Bobines et Mélusine website'
                  />
                </a>
              </Link>
            </motion.figure>

            <div className={style.projectDescContainer}>
              <h3 className={style.projectTitle}>Bobines et Mélusine</h3>
              <time className={style.projectTime} dateTime='2022'>
                2022
              </time>

              <div className={style.projectTech}>
                <FaPhp className={style.techSVG} />
                <FaWordpress className={style.techSVG} />
                <NextImage
                  className={clsxm(style.techSVG, '!w-32')}
                  width={696}
                  height={160}
                  src='/svg/woocommerce.svg'
                  alt='logo Woocommerce'
                />
                <SiTailwindcss className={style.techSVG} />
              </div>
            </div>
          </motion.div>

          {/*Quad Attitude project*/}
          <motion.div
            className={style.projectContainer}
            initial='offscreen'
            whileInView='onscreen'
            viewport={{ once: true, amount: 0.8 }}
          >
            <motion.figure
              className={clsxm(style.projectMockupBg, 'bg-quadAttitude/40')}
              variants={mockupVariants}
            >
              <Link href='https://www.quad-attitude-essaouira.com/'>
                <a>
                  <NextImage
                    className={style.projectMockup}
                    layout='responsive'
                    width={1901}
                    height={1000}
                    src='/images/quad-attitude-essaouira-mockup.png'
                    alt='screenshot of Quad Attitude website'
                  />
                </a>
              </Link>
            </motion.figure>

            <div className={style.projectDescContainer}>
              <h3 className={style.projectTitle}>Quad Attitude Essaouira</h3>
              <time className={style.projectTime} dateTime='2022'>
                2022
              </time>

              <div className={style.projectTech}>
                <FaPhp className={style.techSVG} />
                <FaWordpress className={style.techSVG} />
                <NextImage
                  className={clsxm(style.techSVG, '!w-32')}
                  width={696}
                  height={160}
                  src='/svg/woocommerce.svg'
                  alt='logo Woocommerce'
                />
              </div>
            </div>
          </motion.div>

          {/*Zouina Cheval project*/}
          <motion.div
            className={style.projectContainer}
            initial='offscreen'
            whileInView='onscreen'
            viewport={{ once: true, amount: 0.8 }}
          >
            <motion.figure
              className={clsxm(style.projectMockupBg, 'bg-quadAttitude/40')}
              variants={mockupVariants}
            >
              <Link href='https://zouina-cheval.com/'>
                <a>
                  <NextImage
                    className={style.projectMockup}
                    layout='responsive'
                    width={1902}
                    height={1003}
                    src='/images/zouina-cheval-mockup.png'
                    alt='screenshot of Zouina Cheval website'
                  />
                </a>
              </Link>
            </motion.figure>

            <div className={style.projectDescContainer}>
              <h3 className={style.projectTitle}>Zouina Cheval</h3>
              <time className={style.projectTime} dateTime='2022'>
                2022
              </time>

              <div className={style.projectTech}>
                <FaPhp className={style.techSVG} />
                <FaWordpress className={style.techSVG} />
              </div>
            </div>
          </motion.div>

          {/*NeHo project*/}
          <motion.div
            className={style.projectContainer}
            initial='offscreen'
            whileInView='onscreen'
            viewport={{ once: true, amount: 0.8 }}
          >
            <motion.figure
              className={clsxm(
                style.projectMockupBg,
                'overflow-x-hidden bg-isatis/40'
              )}
              variants={mockupVariants}
            >
              <motion.div
                ref={neho}
                className={clsxm(style.projectMockup, 'flex gap-4')}
                animate={nehoIsInView ? scrollVariants.onView : ''}
                variants={scrollVariants}
              >
                <NextImage
                  className='min-w-full'
                  width={1289}
                  height={810}
                  src='/images/NeHo-consommations.png'
                  alt='screenshot of Panga NeHo app'
                />
                <NextImage
                  className='min-w-full'
                  width={1296}
                  height={814}
                  src='/images/NeHo-consommations-history.png'
                  alt='screenshot of Panga NeHo app consommations history'
                />
              </motion.div>
            </motion.figure>

            <div className={style.projectDescContainer}>
              <h3 className={style.projectTitle}>Panga - NeHo</h3>
              <time className={style.projectTime} dateTime='2021'>
                2021
              </time>

              <div className={style.projectTech}>
                <FaAngular className={style.techSVG} />
                <FaNodeJs className={style.techSVG} />
                <FaGit className={style.techSVG} />
              </div>
            </div>
          </motion.div>

          {/*Isatis Laboratory project*/}
          <motion.div
            className={style.projectContainer}
            initial='offscreen'
            whileInView='onscreen'
            viewport={{ once: true, amount: 0.8 }}
          >
            <motion.figure
              className={clsxm(style.projectMockupBg, 'bg-isatis/40')}
              variants={mockupVariants}
            >
              <Link href='https://laboratoire-isatis.com/'>
                <a>
                  <NextImage
                    className={style.projectMockup}
                    layout='responsive'
                    width={1901}
                    height={1003}
                    src='/images/laboratoire-isatis-mockup.png'
                    alt='screenshot of Isatis Laboratory website'
                  />
                </a>
              </Link>
            </motion.figure>

            <div className={style.projectDescContainer}>
              <h3 className={style.projectTitle}>Isatis Laboratory</h3>
              <time className={style.projectTime} dateTime='2021'>
                2021
              </time>

              <div className={style.projectTech}>
                <FaPhp className={style.techSVG} />
                <FaWordpress className={style.techSVG} />
                <SiJquery className={style.techSVG} />
              </div>
            </div>
          </motion.div>
        </section>
      </div>
    </>
  );
}
