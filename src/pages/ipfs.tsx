import * as React from 'react';
import { ChangeEvent, FormEvent, useState } from 'react';
import { FiChevronRight } from 'react-icons/fi';

import style from './ipfs.module.css';

import NextImage from '@/components/NextImage';
import Seo from '@/components/Seo';

export default function IpfsPage() {
  const [formValues, setFormValues] = useState({
    cid: 'bafybeieb33pqideyl5ncd33kho622thym5rqv6sujrmelcuhkjlf2hdpu4',
    domain: 'ipfs',
  });

  const handleInputChange = (e: FormEvent<HTMLInputElement>) => {
    const target = e.currentTarget;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    setFormValues({
      ...formValues,
      [name]: value,
    });
  };

  const handleSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const { value, name } = e.target;

    setFormValues({
      ...formValues,
      [name]: value,
    });
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    window.open(
      'https://' + formValues.cid + '.' + formValues.domain + '.sygix.fr',
      '_blank'
    );
  };

  return (
    <>
      <Seo
        templateTitle='IPFS Gateway'
        description='Use my personal IPFS gateway to explore data using an http endpoint'
      />

      <div className='flex w-full flex-col items-center'>
        <section className={style.ipfs}>
          <h1>IPFS Gateway</h1>
          <p>
            Explore data available on the public IPFS network using an HTTP
            endpoint. Add your content ID (CID) below to try it out.
          </p>
          <form className={style.form} onSubmit={handleSubmit}>
            <div className={style.inputText}>
              <span>https://</span>
              <input
                type='text'
                placeholder='Enter your cid'
                name='cid'
                value={formValues.cid}
                onChange={handleInputChange}
              />
              <select
                name='domain'
                value={formValues.domain}
                onChange={handleSelectChange}
              >
                <option value='ipfs'>ipfs.sygix.fr</option>
                <option value='ipns'>ipns.sygix.fr</option>
              </select>
            </div>

            <button type='submit'>
              <FiChevronRight />
            </button>
          </form>
          <p>
            I&apos;ve setup my own ipfs gateway for testing purpose, you can use
            it for your personal usage (no illegal activities plz). I do not
            guarantee any availability or performance as it&apos;s not a
            production nor public gateway.
          </p>
          <p>Big Buck Bunny poster queried with this gateway :</p>

          <NextImage
            className={style.projectMockup}
            width={500}
            height={707}
            src='https://bafybeieb33pqideyl5ncd33kho622thym5rqv6sujrmelcuhkjlf2hdpu4.ipfs.sygix.fr/poster.jpg'
            alt='screenshot of Bobines et Mélusine website'
          />
        </section>
      </div>
    </>
  );
}
